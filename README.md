# mermaid-test

Public repo to test Gitlab's builtin `Mermaid` support.

```mermaid
    sequenceDiagram
    User->>Gitlab: Hey, does this work?
    Gitlab-->>User: Sure does!
```