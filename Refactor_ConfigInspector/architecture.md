# Config InspectorArchitecture

```mermaid
classDiagram
    class IssueType{
        <<enumaration>>
        error
        warning
    }

    class ConfigIssue {
        +string message_
        +vector<string> source_
        +IssueType type_
        +getFixer() : optional<ConfigFixer>
    }
    ConfigIssue ..> ConfigFixer
    ConfigIssue ..> IssueType

    class InspectionResult {
        +add(ConfigIssue): void

        -vector<ConfigIssue> issues_
    }
    InspectionResult ..> ConfigIssue

    class InspectorReporter {
        <<abstract>>
        +report(InspectionResult) const = 0 : void
    }
    InspectorReporter ..> InspectionResult

    class UiInspectorReporter {
        +report(InspectionResult) const : void
        +fix(ConfigIssue)
    }
    UiInspectorReporter --|> InspectorReporter : implements

    class LogInspectorReporter {
        +report(InspectionResult) const : void
    }
    LogInspectorReporter --|> InspectorReporter : implements

    class ConfigFixer {
        <<abstract>>
        +fix()
    }

    class ErrorXConfigFixer {
        fix()
    }
    ErrorXConfigFixer --|> ConfigFixer : implements

    class ConfigInspector {
        <<abstract>>
        +inspect(ConfigInspectorReporter&) const = 0 : void
    }
    ConfigInspector ..> InspectionResult

    class VariableFieldConfigInspector {
        +ctor(VariableFieldConfig/Parameters)
        +inspect(ConfigInspectorReporter&) const : void

        -VariableFieldConfig/Parameters config
    }
    VariableFieldConfigInspector --|> ConfigInspector : implements

    class ContinuousModeConfigInspector {
        +ctor(GUIConfig, StreamingConfig)
        +inspect(ConfigInspectorReporter&) const : void

        -GUIConfig gui_config_
        -StreamingConfig streaming_config_
    }
    ContinuousModeConfigInspector --|> ConfigInspector : implements
```
